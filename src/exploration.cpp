#include <base/BaseNode.h>
#include <string>
#include "exploration/exploration.h"

Exploration::Exploration(std::string node_name)
  : BaseNode(node_name){
}

bool Exploration::initialize(){
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  // init variables
  got_waypoint = false;
  got_odom = false;
  hit_waypoint = false;
  
  // init subscribers
  waypoint_sub = nh->subscribe("custom_waypoint", 1, &Exploration::waypoint_callback, this);
  odom_sub = nh->subscribe("odometry", 1, &Exploration::odom_callback, this);

  // init publishers
  hit_waypoint_pub = nh->advertise<std_msgs::Bool>("hit_waypoint", 1);
  
  return true;
}

bool Exploration::execute(){

  if(got_odom && got_waypoint){
    ROS_INFO_STREAM("distance: " << waypoint.distance(odom));
    if(!hit_waypoint)
      hit_waypoint = waypoint.distance(odom) < 4.0f;
    
    std_msgs::Bool hit_waypoint_msg;
    hit_waypoint_msg.data = hit_waypoint;
    hit_waypoint_pub.publish(hit_waypoint_msg);
  }
  
  return true;
}


void Exploration::waypoint_callback(geometry_msgs::PoseStamped msg){
  got_waypoint = true;
  waypoint = tf::Vector3(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);
}

void Exploration::odom_callback(nav_msgs::Odometry msg){
  got_odom = true;
  odom = tf::Vector3(msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z);
}


Exploration::~Exploration(){
}

BaseNode* BaseNode::get(){
  Exploration* exploration = new Exploration("Exploration");
  return exploration;
}
