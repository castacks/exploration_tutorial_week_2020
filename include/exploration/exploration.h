#ifndef _EXPLORATION_H_
#define _EXPLORATION_H_

#include <base/BaseNode.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Bool.h>
#include <string>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

class Exploration : public BaseNode {
private:
  
  // variables
  bool got_waypoint, got_odom;
  tf::Vector3 waypoint, odom;
  bool hit_waypoint;
  
  // publishers
  ros::Publisher hit_waypoint_pub;
  
  // subscribers
  ros::Subscriber waypoint_sub;
  ros::Subscriber odom_sub;
  tf::TransformListener* listener;
  
  // callbacks
  void waypoint_callback(geometry_msgs::PoseStamped msg);
  void odom_callback(nav_msgs::Odometry msg);
  
  
public:
  Exploration(std::string node_name);
  
  virtual bool initialize();
  virtual bool execute();
  virtual ~Exploration();
  
};


#endif
